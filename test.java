
import java.io.File;
import java.io.FileNotFoundException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.sun.xml.internal.ws.org.objectweb.asm.Type;

import java.io.StringWriter;
import java.io.StringReader;
import java.io.BufferedReader;
import java.io.FileReader;

import java.util.Date;

public class test {
  public static void main(String[] args) {
    System.out.println("Hello, world");
    RequestResource msg1 = new RequestResource();
    msg1.setMessageID("urn:au-qld-eoc:12332");
    msg1.getSentDateTime(new Date());
    msg1.setMessageContentType("RequestResource");

    // try {

    // File file = new File("c:\\file.xml");
    // // JAXBContext jaxbContext = JAXBContext.newInstance(RequestResource.class);
    // JAXBContext jaxbContext = JAXBContext.newInstance(msg1.getClass());
    // Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

    // // output pretty printed
    // jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

    // jaxbMarshaller.marshal(msg1, file);
    // jaxbMarshaller.marshal(msg1, System.out);

    // } catch (JAXBException e) {
    // e.printStackTrace();
    // }

    try {
      XmlMgr xmlmgr = new XmlMgr();
      xmlmgr.beanToXml(msg1);
      // XmlMgr.serialize(msg1);
      // System.out.println(xml);
    } catch (Exception ee) {
      System.out.println(ee.getMessage());
    }

    try {
      BufferedReader in = new BufferedReader(new FileReader("c:\\RequestResource.xml"));
      StringBuilder strBuilder = new StringBuilder();
      String str;
      while ((str = in.readLine()) != null) {
        strBuilder.append(str);
      }
      in.close();
      String xml = strBuilder.toString();
      System.out.println(xml);
      XmlMgr xmlmgr = new XmlMgr();
      RequestResource result = xmlmgr.xmlToBean(xml, new RequestResource());

      System.out.println("AuthCode=" + result.getSentDateTime());

    } catch (Exception ee) {
      System.out.println(ee.getMessage());
    }
  }
}
