
// @XmlSchema(namespace="http://www.example.com/a",elementFormDefault=XmlNsForm.QUALIFIED,xmlns={@XmlNs(prefix="ns1",namespaceURI="http://www.example.com/a")})
// package com;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchema;
import javax.xml.bind.annotation.XmlNs;
import java.util.Date;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAccessType;

// @javax.xml.bind.annotation.XmlSchema(xmlns = {
// @XmlNs(prefix = "xsi", namespaceURI =
// "http://www.w3.org/2001/XMLSchema-instance"),
// @XmlNs(prefix = "schemaLocation", namespaceURI =
// "urn:oasis:names:tc:emergency:EDXL:RM:1.0:msg") })
@XmlRootElement(name = "RequestResource", namespace = "urn:oasis:names:tc:emergency:EDXL:RM:1.0:msg")
@XmlAccessorType(XmlAccessType.FIELD)
public class RequestResource {

  @XmlElement(name = "MessageID")
  private String messageID;

  @XmlElement(name = "SentDateTime")
  @XmlJavaTypeAdapter(DateTimeAdapter.class)
  private Date sentDateTime;

  @XmlElement(name = "MessageContentType")
  private String messageContentType;

  public String getMessageID() {
    return messageID;
  }

  public void setMessageID(String messageID) {
    this.messageID = messageID;
  }

  public Date getSentDateTime() {
    return sentDateTime;
  }

  public void getSentDateTime(Date sentDateTime) {
    this.sentDateTime = sentDateTime;
  }

  public String getMessageContentType() {
    return messageContentType;
  }

  public void setMessageContentType(String messageContentType) {
    this.messageContentType = messageContentType;
  }

}