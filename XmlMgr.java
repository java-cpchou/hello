import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
// import java.lang.reflect.GenericDeclaration;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.io.File;
import java.io.FileNotFoundException;

public class XmlMgr {

  public XmlMgr() {

  }

  /**
   * XML 转转为 JavaBean
   *
   * @param xml
   * @param t
   * @param <T>
   * @return
   * @throws JAXBException
   */
  public <T> T xmlToBean(String xml, T t) throws JAXBException {
    JAXBContext context = JAXBContext.newInstance(t.getClass());
    Unmarshaller um = context.createUnmarshaller();
    StringReader sr = new StringReader(xml);
    t = (T) um.unmarshal(sr);
    return t;
  }

  // public <T> void beanToXml2(T t) throws JAXBException {
  // JAXBContext context = JAXBContext.newInstance(t.getClass());
  // Marshaller m = context.createMarshaller();
  // StringWriter sw = new StringWriter();
  // m.marshal(t, sw);
  // m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
  // // m.marshal(t, new FileOutputStream("c:\\test.xml"));
  // m.marshal(t, System.out);
  // // return sw;
  // }

  public <T> StringWriter beanToXml(T t) throws JAXBException, FileNotFoundException {
    JAXBContext context = JAXBContext.newInstance(t.getClass());
    Marshaller m = context.createMarshaller();
    StringWriter sw = new StringWriter();
    m.marshal(t, sw);
    m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
    File m_file = new File("c:\\RequestResource.xml");
    m.marshal(t, m_file);

    m.marshal(t, System.out);

    return sw;
  }

  // public static String serialize(RequestResource msg) {

  // StringWriter sw = beanToXml(msg);
  // System.out.println(sw.toString());
  // }

}